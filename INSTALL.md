# Installation Guide


```sh
gitbook build
```


for the editor see also:
[gitbook-legacy-editor](https://gitlab.com/holokit/holokit.gitlab.io/-/tree/master/gitbook-legacy-editor)


### deploy code for gl-pages ([[.gitlab-ci.yml]])
```yaml
pages:
  stage: deploy
  script:
    - bundle exec jekyll build -d public
  artifacts:
    paths:
      - public
  only:
    - master

```

